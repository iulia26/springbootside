package com.example.serverApp;

import com.example.serverApp.enumeration.Status;
import com.example.serverApp.model.Server;
import com.example.serverApp.repo.ServerRepo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.sql.Array;
import java.util.Arrays;

import static com.example.serverApp.enumeration.Status.SERVER_DOWN;
import static com.example.serverApp.enumeration.Status.SERVER_UP;

@SpringBootApplication
public class ServerAppApplication {

	public static void main(String[] args) {

		SpringApplication.run(ServerAppApplication.class, args);

	}

	@Bean
	CommandLineRunner run(ServerRepo serverRepo){

		return args -> {
			Server srv1 = new Server(null,"172.17.0.1","Ubuntu","16 GB","Personal PC", "http://localhost:8080/serverApp/image/server1.png", SERVER_DOWN);
			serverRepo.save(srv1);
			serverRepo.save(new Server(null,"192.168.3.240","Web Server","32 GB","Personal PC", "http://localhost:8080/serverApp/image/server2.png", SERVER_UP));
			serverRepo.save(new Server(null,"10.0.1.254","Dell tower","16 GB","Personal PC", "http://localhost:8080/serverApp/image/server3.png", SERVER_UP));
			serverRepo.save(new Server(null,"192.168.3.1","Ubb cluj","64 GB","Personal PC", "http://localhost:8080/serverApp/image/server4.png", SERVER_DOWN));
		};
	}

	@Bean
	public CorsFilter corsFilter(){
		UrlBasedCorsConfigurationSource urlBasedCorsConfigurationSource  = new UrlBasedCorsConfigurationSource();
		CorsConfiguration corsConfiguration = new CorsConfiguration();
		corsConfiguration.setAllowCredentials(true);
		corsConfiguration.setAllowedOrigins(Arrays.asList("http://localhost:3000","http://localhost:4200"));
		corsConfiguration.setAllowedHeaders(Arrays.asList("Origin","Access-Control-Allow-Origin","Content-Type","Accept","Jwt-Token","Authorization","Origin, Accept","X-Requested-With","Access-Control-Request-Method"
		,"Access-Control-Request-Headers"));
		corsConfiguration.setExposedHeaders(Arrays.asList("Origin","Content-Type","Accept","Jwt-Token","Authorization","Access-Control-Allow-Origin","Access-Control-Allow-Origin","Access-Control-Allow-Credentials","Filename"));
		corsConfiguration.setAllowedMethods(Arrays.asList("GET","PUT","POST","PATCH","DELETE","OPTIONS"));
		urlBasedCorsConfigurationSource.registerCorsConfiguration("/**",corsConfiguration);
		return new CorsFilter(urlBasedCorsConfigurationSource);

	}
}
