package com.example.serverApp.service.implementation;

import com.example.serverApp.enumeration.Status;
import com.example.serverApp.model.Server;
import com.example.serverApp.repo.ServerRepo;
import com.example.serverApp.service.ServerService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Random;

@RequiredArgsConstructor //lomback will create constructor and it's gonna put serverRepo parameter there for us
@Service
@Transactional
@Slf4j  //it's a log,allows us to print something in the console to check what it's happening
public class ServerServiceImplementation implements ServerService {

    private final ServerRepo serverRepo;

    @Override
    public Server create(Server server) {
        log.info("Saving new server: {}",server.getName());
        server.setImageUrl(retrieveServerImageUrl());
        return serverRepo.save(server);
    }

    @Override
    public Server ping(String ipAddress) throws IOException {
        log.info("Ping server IP : {}",ipAddress);
        Server server = serverRepo.findByIpAddress(ipAddress);
        InetAddress address = InetAddress.getByName(ipAddress);
        server.setStatus(address.isReachable(10000) ? Status.SERVER_UP : Status.SERVER_DOWN);
        serverRepo.save(server);
        return server;
    }

    @Override
    public Collection<Server> list(int limit) {
        log.info("Fetching all servers paginated");
        return serverRepo.findAll(PageRequest.of(0,limit)).toList();
    }

    @Override
    public Server get(Long id) {
        log.info("Fetching server by id: {}",id);
        return serverRepo.findById(id).get();
    }

    @Override
    public Server update(Server server) {
        log.info("Updating server : {}",server.getName());
        return serverRepo.save(server);
    }

    @Override
    public Boolean delete(Long id) {
        log.info("Delete server by ID : {}",id);
        serverRepo.deleteById(id);
        return Boolean.TRUE;
    }

    private String retrieveServerImageUrl() {
        String[] imageNames = { "server1.png","server2.png","server3.png","server4.png" };
        return ServletUriComponentsBuilder
                .fromCurrentContextPath()
                .path("/serverApp/image/" + imageNames[new Random().nextInt(4)])
                .toUriString();
    }

}
