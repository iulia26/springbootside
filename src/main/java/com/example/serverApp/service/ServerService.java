package com.example.serverApp.service;

import com.example.serverApp.model.Server;

import java.io.IOException;
import java.net.UnknownHostException;
import java.security.cert.CollectionCertStoreParameters;
import java.util.Collection;

public interface ServerService {

    Server create(Server server);

    /**
     *
     * @param ipAddress
     * @return the Server that it's trying to ping
     */
    Server ping(String ipAddress) throws IOException;
    Collection<Server> list(int limit);
    Server get(Long id);
    Server update(Server server);
    Boolean delete(Long id);
}
