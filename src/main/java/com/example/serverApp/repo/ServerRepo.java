package com.example.serverApp.repo;

import com.example.serverApp.model.Server;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServerRepo extends JpaRepository<Server,Long> {


    //Jpa o sa interpreteze numele,deci sa fiu atenta la ce nume atribui parametrilor formali
    Server findByIpAddress(String ipAddress);
}
